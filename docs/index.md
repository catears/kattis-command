# Kattcmd Docs

The documentation is split up into two parts. The tutorials and the
manual. The manual will look at each command and show you what it
does, when it is useful and tips about using it. The tutorials will
focus on helping with a specific issue, similar to an F.A.Q.

## Tutorial

* [Submit your first problem](docs/first-problem-tutorial.md)

* [Configure kattcmd with user values](docs/configure-kattcmd.md)

* [Writing a plugin for kattcmd](docs/writing-a-plugin.md)

## Manuals

* [tips](docs/tips.md)

* [init](docs/init.md)

* [open](docs/open.md)

* [test](docs/test.md)

* [submit](docs/submit.md)

* [compile](docs/compile.md)

* [clean](docs/clean.md)

* [getval/setval](docs/getsetval.md)
