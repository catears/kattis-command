# Kattis-Command

Tool for managing your competitive programming library and kattis solutions

Do you want to skip submitting manually to kattis? Do you want to have
a clean and nice structure for all your files when solving problems?
Are you willing to support and use community created tools?


## Main Goals

* Easy to install, easy to use

* Manage all non-programming related processes when working with Kattis

* Easily extendable with plugins


## Setup

Install through the use of pip. However you need to install it through
pip3, which may not be installed on your computer. If you have apt-get
installed you can run

```
$ sudo apt-get install python3-pip
```

After that you can install it with

```
$ sudo -H pip3 install kattcmd
```

Or you can use `virtualenv`, but then you will need it whenever you
want to use `kattcmd`.

## Usage

See instructions on commands inside [docs](docs/index.md)

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)


## Credits

